package packColors;

public class Colors {
    static String cyan="\033[1;36m";       /* 1 -> negrita ;  36 -> cyan */
    static String verd="\033[0;32m";        /* 4 -> subratllat ;  32 -> green */
    static String blau="\033[9;34m";       /* 9 -> strike ;  34 -> blue */;
    static String negre="\033[0;30m";
    static String marro="\033[1;33m";
    static String magenta="\033[0;35m";
    static String gris="\033[0;37m";
}
