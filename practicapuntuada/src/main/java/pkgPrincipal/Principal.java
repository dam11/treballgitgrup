package pkgPrincipal;


import pkgJavaMail.JavaMail;

import static pkgFinestres.Finestres.IntrodueixDades;
import static pkgFinestres.Finestres.SeleccioOpcions;

public class Principal {
    public static void main(String[] args) {

        // variables
        String desti="";
        String missatge;
        String assumpte;
        String password;
        String fitxer=null;

        // important crear una variable de la classe on tenim les funcions del correu
        // encara que utilitzarem les funcions estàtiques

        JavaMail j=new JavaMail();         // CREEM UNA VARIABLE DE LA CLASSE

        assumpte=IntrodueixDades("Assumpte del correu");
        missatge=IntrodueixDades("Missatge del correu");

        String v[]={"Si","No"};
        String op=SeleccioOpcions(v,"Adjunt?","Voleu adjuntar algun fitxer?");

        if (op.equals("Si")){
            fitxer=EscullFitxer("~","escull el fitxer adjunt");
        }

        do {
            desti = IntrodueixDades("Dona'm el destí");
            if (!desti.contains("@") || !desti.contains(".")) {
                MissatgeError("El destí ha de tenir el format correu electrònic");
                desti = "";
            }
        }while (desti=="");

        password=IntrodueixPassword("Password");
        CrearMissatgeAmbAdjunt(desti,missatge,assumpte,fitxer);
        EnviarCorreu(password);
        MissatgeInfo("Missatge Enviat!");






    }

}
