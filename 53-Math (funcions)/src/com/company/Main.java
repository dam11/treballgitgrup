package com.company;

public class Main {

    public static void main(String[] args) {


        System.out.println("La sinus de 3 és: " + Math.rint(Math.sin(3) * 10000) / 10000 +
                            "\nLa cosinus de 3 és: " + Math.rint(Math.cos(3) * 10000) / 10000 +
                            "\nL'arrel quadrada de 3 és: " + Math.rint(Math.sqrt(3) * 10000) / 10000 +
                            "\nLa potencia de 3⁴ és: " + Math.rint(Math.pow(3, 4) * 10000) / 10000);

    }
}
