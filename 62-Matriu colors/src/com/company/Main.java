package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    /* 0 -> normal ;  31 -> red */
    /* 1 -> negrita ;  36 -> cyan */
    /* 4 -> subratllat ;  32 -> green */
    /* 9 -> strike ;  34 -> blue */

    static Random rnd = new Random();
    static String roig="\033[0;31m";
    static String blau="\033[0;34m";
    static String verd="\033[0;32m";
    static String nc="\033[0m";
    static String subratllat="\033[4;31m";

    static int[][] creaMatriu(int n, int k) {
        int i, j, max = 10;
        int m[][] = new int[n][k];

        for (i = 0; i < n; i++) {
            for (j = 0; j < k; j++) {

                m[i][j] = rnd.nextInt(max);
            }
        }
        return m;
    }
    static void imprimeixMatriu(int[][] m) {

        int i, j;
        for (i = 0; i < m.length; i++) {
            for (j = 0; j < m[i].length; j++) {

                if (m[i][j]%2==0){
                    System.out.print(blau+m[i][j]+nc+" ");
                }else{
                    System.out.print(verd+m[i][j]+nc+" ");
                }
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int max = 10;
        int[][] m;

        System.out.println(roig+"\t   MATRIU\n"+nc+"      "+subratllat+"\t\t  \n"+nc);
        m = creaMatriu(max, max);
        imprimeixMatriu(m);
    }
}
