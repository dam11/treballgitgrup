package pkgFuncions;

import java.util.Scanner;

public class Funcions {

    public static Scanner scan = new Scanner(System.in);

    public static char[][] crear_taulell(int max) {

        int i, j;
        char m[][] = new char[max][max];

        for (i = 0; i < max; i++) {
            for (j = 0; j < max; j++) {

                m[i][j] = '-';
            }
        }
        return m;
    }

    public static void imprimir_taulell(char[][] quadre, char fitxa) {
        int i, j;
        System.out.println("\n\nTorn de: " + fitxa);

        for (i = 0; i < quadre.length; i++) {
            for (j = 0; j < quadre[i].length; j++) {

                System.out.print(quadre[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static char escull_fitxa(char fitxa) {

        if (fitxa == 'X') {
            return 'O';
        } else {
            return 'X';
        }
    }

    public static char[][] fer_tirada(char[][] quadre, char fitxa) {
        int columna, fila, i, j;
        do {
            System.out.println("\nDona'm la columna:");
            columna = scan.nextInt();
            System.out.println("\nDona'm la fila:");
            fila = scan.nextInt();
        } while (quadre[columna][fila] != '-');

        for (i = 0; i < quadre.length; i++) {
            for (j = 0; j < quadre[i].length; j++) {
                if (i == columna && j == fila) {
                    quadre[i][j] = fitxa;
                } else {
                }
            }
        }
        System.out.println("");
        return quadre;
    }

    public static boolean comprova_guanyar(char[][] quadre) {
        int i, j;
        boolean guanyador;
        for (i = 0; i < quadre.length; i++) {
            for (j = 0; j < quadre[i].length; j++) {

                if (quadre[0][0] == 'X' && quadre[0][1] == 'X' && quadre[0][2] == 'X') {
                    return true;
                }
                if (quadre[1][0] == 'X' && quadre[1][1] == 'X' && quadre[1][2] == 'X') {
                    return true;
                }
                if (quadre[2][0] == 'X' && quadre[2][1] == 'X' && quadre[2][2] == 'X') {
                    return true;
                }
                if (quadre[0][0] == 'X' && quadre[1][0] == 'X' && quadre[2][0] == 'X') {
                    return true;
                }
                if (quadre[0][1] == 'X' && quadre[1][1] == 'X' && quadre[2][1] == 'X') {
                    return true;
                }
                if (quadre[0][2] == 'X' && quadre[1][2] == 'X' && quadre[2][2] == 'X') {
                    return true;
                }
                if (quadre[0][0] == 'X' && quadre[1][1] == 'X' && quadre[2][2] == 'X') {
                    return true;
                }
                if (quadre[0][2] == 'X' && quadre[1][1] == 'X' && quadre[2][0] == 'X') {
                    return true;
                }


                if (quadre[0][0] == 'O' && quadre[0][1] == 'O' && quadre[0][2] == 'O') {
                    return true;
                }
                if (quadre[1][0] == 'O' && quadre[1][1] == 'O' && quadre[1][2] == 'O') {
                    return true;
                }
                if (quadre[2][0] == 'O' && quadre[2][1] == 'O' && quadre[2][2] == 'O') {
                    return true;
                }
                if (quadre[0][0] == 'O' && quadre[1][0] == 'O' && quadre[2][0] == 'O') {
                    return true;
                }
                if (quadre[0][1] == 'O' && quadre[1][1] == 'O' && quadre[2][1] == 'O') {
                    return true;
                }
                if (quadre[0][2] == 'O' && quadre[1][2] == 'O' && quadre[2][2] == 'O') {
                    return true;
                }
                if (quadre[0][0] == 'O' && quadre[1][1] == 'O' && quadre[2][2] == 'O') {
                    return true;
                }
                if (quadre[0][2] == 'O' && quadre[1][1] == 'O' && quadre[2][0] == 'O') {
                    return true;
                }
            }
        }
        return false;
    }

}
