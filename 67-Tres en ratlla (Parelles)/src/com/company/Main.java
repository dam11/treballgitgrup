package com.company;

import java.sql.SQLOutput;
import java.util.Scanner;

import static pkgFuncions.Funcions.*;

public class Main {
    static Scanner scan = new Scanner(System.in);
    public static void main(String[] args) {
        int max = 3;
        int i, j;
        char[][] quadre = new char[3][3];
        int jugador = 1;
        char fitxa = 'X';
        int jugades = 0;
        int fila, columna;
        boolean guanyador;
        Scanner scan = new Scanner(System.in);


        quadre = crear_taulell(max);
        do {

            fitxa = escull_fitxa(fitxa);
            imprimir_taulell(quadre, fitxa);
            quadre = fer_tirada(quadre, fitxa);
            jugades++;
            guanyador = comprova_guanyar(quadre);

        } while (jugades < 9 && !guanyador);


        if (!guanyador) {
            System.out.println("\n\n\n JOC ACABAT, NO HA GUANYAT NINGÚ.");
        } else {
            System.out.println("\n\n\n JOC ACABAT, HA GUANYAT: "+fitxa);
        }
        imprimir_taulell(quadre, fitxa);
    }
}
