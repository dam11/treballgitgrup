package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    static Scanner scan = new Scanner(System.in);
    static Random rnd = new Random();

    static int[] crea_vector(int n) {

        int i;
        int[] v = new int[n];

        for (i = 0; i < n; i++) {
            v[i] = rnd.nextInt(n);
        }
        return v;
    }

    static void Imprimeix_vector(int[] v) {
        int i;

        for (i = 0; i < v.length; i++) {

            System.out.println("v[" + i + "] =" + v[i]);
        }
    }

    public static void main(String[] args) {
        int max;

        System.out.print("Quina grandària té el vector?\n");
        max = scan.nextInt();
        int[] v;

        v = crea_vector(max);
        Imprimeix_vector(v);
    }
}
