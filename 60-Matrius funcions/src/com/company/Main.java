package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    static Random rnd = new Random();

    static int[][] creaMatriu(int n, int k) {
        int i, j, max = 10;
        int m[][] = new int[n][k];

        for (i = 0; i < n; i++) {
            for (j = 0; j < k; j++) {

                m[i][j] = rnd.nextInt(max);
            }
        }
        return m;
    }
    static void imprimeixMatriu(int[][] m) {

        int i, j;
        for (i = 0; i < m.length; i++) {
            for (j = 0; j < m[i].length; j++) {
                System.out.print(m[i][j]+" ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int max = 5;
        int[][] m;
        m = creaMatriu(max, max);
        imprimeixMatriu(m);
    }
}

