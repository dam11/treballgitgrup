package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    static Scanner scan = new Scanner(System.in);
    static Random rnd = new Random();

    //<editor-fold defaultstate="collapsed" desc="FUNCIONS">

    static int[] crea_vector(int n) {

        int i;
        int[] v = new int[n];

        for (i = 0; i < n; i++) {
            v[i] = rnd.nextInt(n);
        }
        return v;
    }

    static void Imprimeix_vector(int[] v) {
        int i;

        for (i = 0; i < v.length; i++) {

            System.out.println("v[" + i + "] =" + v[i]);
        }
    }

    static boolean buscaNum(int[] v, int b) {
        int i;
        int num = 0;
        for (i = 0; i < v.length; i++) {
            if (v[i] == b) {
                num++;
            } else {

            }
        }
        if (num != 0) {
            return true;
        } else {
            return false;
        }
    }

    //</editor-fold>


    public static void main(String[] args) {
        int max, num;
        boolean trobat;

        System.out.print("Quina grandària té el vector?\n");
        max = scan.nextInt();
        int[] v;

        v = crea_vector(max);
        Imprimeix_vector(v);

        System.out.println("Quin número vols buscar?");
        num = scan.nextInt();

        trobat = buscaNum(v, num);

        if (trobat) {
            System.out.println("El número "+num+" SI està en el vector.");

        }else{
            System.out.println("El número "+num+" NO està en el vector.");

        }
    }
}
