package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    /* 0 -> normal ;  31 -> red */
    /* 1 -> negrita ;  36 -> cyan */
    /* 4 -> subratllat ;  32 -> green */
    /* 9 -> strike ;  34 -> blue */

    static Random rnd = new Random();
    static String roig = "\033[1;31m";
    static String cyan = "\033[1;36m";
    static String verd = "\033[1;32m";
    static String blau = "\033[1;34m";
    static String negre = "\033[1;30m";
    static String marro = "\033[1;33m";
    static String magenta = "\033[1;35m";
    static String gris = "\033[1;37m";

    static String nc = "\033[0m";


    static int[][] creaMatriu(int n, int k) {
        int i, j, max = 10;
        int m[][] = new int[n][k];

        for (i = 0; i < n; i++) {
            for (j = 0; j < k; j++) {

                m[i][j] = rnd.nextInt(max);
            }
        }
        return m;
    }

    static void imprimeixMatriu(int[][] m, String[] z) {

        int i, j;
        for (i = 0; i < m.length; i++) {
            for (j = 0; j < m[i].length; j++) {

                System.out.print(z[rnd.nextInt(z.length)] + m[i][j] + nc + " ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int max = 10;
        int[][] m;
        String[] z = {roig, verd, blau, magenta, marro, negre, gris, cyan};

        System.out.println(roig + "\t   MATRIU\n" + nc + "      " + "\t\t  \n" + nc);


        m = creaMatriu(max, max);
        imprimeixMatriu(m, z);
    }
}
