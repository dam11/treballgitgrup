package com.company;

import java.util.Random;

public class Main {

    static Random rnd = new Random();

    static int[] crea_vector(int n) {
        int i;
        int[] v = new int[n];
        for (i = 0; i < n; i++) {
            v[i] = rnd.nextInt(n);
        }
        return v;
    }

    static void Imprimeix_vector(int[] v) {
        int i;
        for (i = 0; i < v.length; i++) {
            System.out.print(v[i] + ", ");
        }
    }

    static int[] borraParells(int[] v) {
        int i;
        for (i = 0; i < v.length; i++) {

            while (v[i] % 2 == 0) {
                v[i] = rnd.nextInt(20);
            }
        }
        return v;
    }


    public static void main(String[] args) {
        int max = 20;
        int[] vec;
        int[] vec2;

        vec = crea_vector(max);
        vec2 = vec;
        System.out.print("\n\nVector original:");
        Imprimeix_vector(vec);

        vec2 = borraParells(vec2);
        System.out.print("\n\nVector canviat:");
        Imprimeix_vector(vec2);
        System.out.print("\n\n\n");
    }
}
