package com.company;

import java.util.Scanner;

public class Main {

    static Scanner scan = new Scanner(System.in);

    private static int operacio(int x) {
        int total;
        total = (x * x * x * x - x * x + 1);
        return total;
    }

    public static void main(String[] args) {

        int x, total;

        System.out.println("Dona'm un numero:");
        x = scan.nextInt();

        System.out.println(x + "⁴-" + x + "²+1 = " + operacio(x));

    }
}
