package com.company;

public class Main {

    private static double total(double x, double iva) {
        double total;
        total = ((x * iva) / 100) + x;
        return total;
    }

    public static void main(String[] args) {

        if (args.length != 2) {
            System.out.println("Error, has de de posar 2 paràmentres.");
        } else {

            double x = Integer.parseInt(args[0]);
            double iva = Integer.parseInt(args[1]);

            System.out.println("El " + iva + "% d'IVA de " + x + "€ és: " + total(x, iva));
        }
    }
}
