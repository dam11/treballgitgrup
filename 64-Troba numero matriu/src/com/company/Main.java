package com.company;

import com.oracle.xmlns.internal.webservices.jaxws_databinding.SoapBindingParameterStyle;
import com.sun.deploy.net.proxy.SunAutoProxyHandler;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.sql.SQLOutput;
import java.util.Random;
import java.util.Scanner;

public class Main {

        // <editor-fold defaultstate="collapsed" desc="FUNCIONS">

    /* 0 -> normal ;  31 -> red */
    /* 1 -> negrita ;  36 -> cyan */
    /* 4 -> subratllat ;  32 -> green */
    /* 9 -> strike ;  34 -> blue */

    static Scanner scan = new Scanner(System.in);
    static int numero;
    static Random rnd = new Random();
    static String roig = "\033[1;31m";
    static String cyan = "\033[1;36m";
    static String verd = "\033[1;32m";
    static String blau = "\033[1;34m";
    static String negre = "\033[1;30m";
    static String marro = "\033[1;33m";
    static String magenta = "\033[1;35m";
    static String gris = "\033[1;37m";
    static String nc = "\033[0m";

    static String subratllat = "\033[4m";


    static int[][] creaMatriu(int n, int k) {
        int i, j, max = 10;
        int m[][] = new int[n][k];

        for (i = 0; i < n; i++) {
            for (j = 0; j < k; j++) {

                m[i][j] = rnd.nextInt(max);
            }
        }
        return m;
    }

    static void imprimeixMatriu(int[][] m) {

        int i, j;
        for (i = 0; i < m.length; i++) {
            for (j = 0; j < m[i].length; j++) {

                System.out.print(m[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    static void imprimeixMatriu2(int[][] m, int num) {

        int i, j, cont = 0;
        for (i = 0; i < m.length; i++) {
            for (j = 0; j < m[i].length; j++) {
                if (m[i][j] == num) {
                    if (cont == 0) {
                        System.out.print(blau + m[i][j] + " " + nc);
                        cont++;
                    } else {
                        System.out.print(subratllat + " " + nc + " " + nc);
                    }
                } else {
                    System.out.print(subratllat + " " + nc + " " + nc);
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    static boolean buscanum(int[][] m, int num) {
        int i, j;
        for (i = 0; i < m.length; i++) {
            for (j = 0; j < m[i].length; j++) {
                if (m[i][j] == num) {
                    return true;
                }
            }
        }
        return false;
    }

    // </editor-fold>

    public static void main(String[] args) {
        int max = 10;
        int[][] m;

        String[] z = {roig, verd, blau, magenta, marro, negre, gris, cyan};

        System.out.println("Dona'm un numero:");
        numero = scan.nextInt();

        System.out.println(roig + "\t   MATRIU\n" + nc + "      " + "\t\t  \n" + nc);

        m = creaMatriu(max, max);
        imprimeixMatriu(m);
        if(buscanum(m,numero)==true)
        imprimeixMatriu2(m, numero);
        else
            System.out.println("El numero "+numero+" no hi es");

    }
}

