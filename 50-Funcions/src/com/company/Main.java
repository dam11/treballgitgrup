package com.company;

public class Main {

    private static String signe() {
        return "!";
    }

    private static void mon(String a, String b) {
        System.out.println(a + b + "\n\n");
    }

    private static void hola() {
        String paraula = "mon";
        String var = signe();
        System.out.print("\nhola ");
        mon(paraula, var);
    }


    public static void main(String[] args) {
        hola();
    }
}
