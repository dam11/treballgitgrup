package packMatrius;

import java.util.Random;

public class Matrius {
    public static Random rnd = new Random();

    public static int[][] creaMatriu(int n, int k) {
        int i, j, max = 10;
        int m[][] = new int[n][k];

        for (i = 0; i < n; i++) {
            for (j = 0; j < k; j++) {

                m[i][j] = rnd.nextInt(max);
            }
        }
        return m;
    }

    public static void imprimeixMatriu(int[][] m) {

        int i, j;
        for (i = 0; i < m.length; i++) {
            for (j = 0; j < m[i].length; j++) {
                System.out.print(m[i][j] + " ");
            }
            System.out.println();
        }


    }

    public static char[][] crear_taulell(int max) {

        int i, j;
        char m[][] = new char[max][max];

        for (i = 0; i < max; i++) {
            for (j = 0; j < max; j++) {

                m[i][j] = '-';
            }
        }
        return m;
    }

    public static void imprimir_taulell(char[][] quadre, char fitxa) {
        int i, j;
        System.out.println("\n\nTorn de: " + fitxa);

        for (i = 0; i < quadre.length; i++) {
            for (j = 0; j < quadre[i].length; j++) {

                System.out.print(quadre[i][j] + " ");
            }
            System.out.println("");
        }
    }
}