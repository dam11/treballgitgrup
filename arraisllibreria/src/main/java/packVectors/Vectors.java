package packVectors;

import java.util.Random;
import java.util.Scanner;

public class Vectors {

    public static Scanner scan = new Scanner(System.in);

    public static Random rnd = new Random();

    public static int[] crea_vector(int n) {

        int i;
        int[] v = new int[n];

        for (i = 0; i < n; i++) {
            v[i] = rnd.nextInt(n);
        }
        return v;
    }

    public static void Imprimeix_vector(int[] v) {
        int i;

        for (i = 0; i < v.length; i++) {

            System.out.println("v[" + i + "] =" + v[i]);
        }
    }


}



