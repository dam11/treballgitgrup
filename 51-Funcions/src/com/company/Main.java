package com.company;

import java.util.Scanner;

public class Main {

    static Scanner scan = new Scanner(System.in);

    private static int suma(int a, int b) {
        int total;
        total = a + b;
        return total;
    }

    private static void pregunta(String a) {
        System.out.println("Dona'm el " + a + " número:");
    }

    public static void main(String[] args) {
        int numero, numero2, total;

        pregunta("primer");
        numero = scan.nextInt();
        pregunta("segon");
        numero2 = scan.nextInt();
        total = suma(numero, numero2);

        System.out.println(numero + " + " + numero2 + " = " + total);

    }
}
