package com.company;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {

        // VARIABLES

        int opcio;
        double a, b;


        // RESTA CODI

        do {
            opcio = Integer.parseInt(JOptionPane.showInputDialog(null, "\nMENÚ CALCULADORA\n\n 1) Sumar \n 2) Restar \n 3) Multiplicar \n 4) Dividir \n 5) Potencia \n 6) Arrel quadrada \n 7) Sinus \n 8) Cosinus \n 9) Sortir\n\n", "ESCRIU EL NUMERO", 3));

            switch (opcio) {

                case 1:
                    a = Integer.parseInt(JOptionPane.showInputDialog(null, "Dona'm el primer número", "Suma", 3));
                    b = Integer.parseInt(JOptionPane.showInputDialog(null, "Dona'm el segon número", "Suma", 3));
                    JOptionPane.showMessageDialog(null, "El resultat de la suma:\n" + a + "+" + b + " és: " + (a + b));
                    break;

                case 2:
                    a = Integer.parseInt(JOptionPane.showInputDialog(null, "Dona'm el primer número", "Resta", 3));
                    b = Integer.parseInt(JOptionPane.showInputDialog(null, "Dona'm el segon número", "Resta", 3));
                    JOptionPane.showMessageDialog(null, "El resultat de la resta:\n" + a + "-" + b + " és: " + (a - b));
                    break;

                case 3:
                    a = Integer.parseInt(JOptionPane.showInputDialog(null, "Dona'm el primer número", "Multiplicació", 3));
                    b = Integer.parseInt(JOptionPane.showInputDialog(null, "Dona'm el segon número", "Multiplicació", 3));
                    JOptionPane.showMessageDialog(null, "El resultat de la multiplicació:\n" + a + "*" + b + " és: " + Math.rint((a * b) * 10000) / 10000);
                    break;

                case 4:
                    a = Integer.parseInt(JOptionPane.showInputDialog(null, "Dona'm el primer número", "Divisió", 3));
                    b = Integer.parseInt(JOptionPane.showInputDialog(null, "Dona'm el segon número", "Divisió", 3));
                    JOptionPane.showMessageDialog(null, "El resultat de la divisió:\n" + a + "/" + b + " és: " + Math.rint((a / b) * 10000) / 10000);
                    break;

                case 5:
                    a = Integer.parseInt(JOptionPane.showInputDialog(null, "Dona'm el  número que vols elevar:", "Potencia", 3));
                    b = Integer.parseInt(JOptionPane.showInputDialog(null, "Dona'm el número al qual el vols elevar:", "Potencia", 3));
                    JOptionPane.showMessageDialog(null, "El resultat de la potencia:\n" + a + "^" + b + " és: " + Math.rint(Math.pow(a, b) * 10000) / 10000);
                    break;

                case 6:
                    a = Integer.parseInt(JOptionPane.showInputDialog(null, "Dona'm el  número que vols trobar l'arrel:", "Arrel quadrada", 3));
                    JOptionPane.showMessageDialog(null, "El resultat de l'arrel de " + a + " és: " + Math.rint(Math.sqrt(a) * 10000) / 10000);
                    break;

                case 7:
                    a = Integer.parseInt(JOptionPane.showInputDialog(null, "Dona'm un numero:", "Sinus", 3));
                    JOptionPane.showMessageDialog(null, "El sinus de " + a + " és: " + Math.rint(Math.sin(a) * 10000) / 10000);
                    break;

                case 8:
                    a = Integer.parseInt(JOptionPane.showInputDialog(null, "Dona'm un numero:", "Cosinus", 3));
                    JOptionPane.showMessageDialog(null, "El cosinus de " + a + " és: " + Math.rint(Math.cos(a) * 10000) / 10000);
                    break;

                case 9:
                    JOptionPane.showMessageDialog(null, "Adeu.", "Final", 0);
                    break;

                default:
                    JOptionPane.showMessageDialog(null, "Només pot ser del 1 al 9", "Error", 0);
            }
        } while (opcio != 9);
    }
}
